function calcul(event)
{
    let today = new Date(); 
    if(today>=event)
    { 
        event.setFullYear(event.getFullYear()+1); 
    }
    var one_day=1000*60*60*24;
    return Math.ceil((event.getTime()-today.getTime())/(one_day));
}

class EventDays  
{
    
    Christmas()
    {       
       this.today = new Date();
        let days=calcul(new Date(this.today.getFullYear(), 11, 25)); 
        return (days+ " jour(s) restant avant Noël");
    };

    Hollidays()
    {
        this.today = new Date();
        let days=calcul(new Date(this.today.getFullYear(), 6, 1)); 
        return (days+ " jour(s) restant avant les vancances d'été");
    }

    All(ZeDate)
    {
        this.today = new Date();
        
        let days=calcul(new Date(this.today.getFullYear(), ZeDate.getMonth(), ZeDate.getDate())); 
        return (days+ " jour(s) avant l'événement");
    }
}
export default EventDays;